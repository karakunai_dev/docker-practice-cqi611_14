import config from 'dotenv/config'
import express from 'express'
import bodyParser from 'body-parser'
import { PostgrestClient } from '@supabase/postgrest-js'
import cors from 'cors'

const api_service = express()
const global_port = process.env.EXPOSED_PORT_GATEWAY_API || 3001
const raw_server_url = process.env.GLOBAL_BASE_URL
const raw_server_port = process.env.EXPOSED_PORT_DATABASE_API
const global_url = `${raw_server_url}:${raw_server_port}`
const postgrest = new PostgrestClient(global_url, { schema: "public"})

const post_mahasiswa_create = async (mahasiswa) => {
    const { data, error } = await postgrest
        .from('table_mahasiswa')
        .insert([
            { 
                mahasiswa_nama: `${mahasiswa.nama}`, 
                mahasiswa_gender: `${mahasiswa.gender}`, 
                mahasiswa_birthdate: `${mahasiswa.birth}`, 
                mahasiswa_period: mahasiswa.period, 
                mahasiswa_faculty: `${mahasiswa.faculty}`, 
                mahasiswa_major: `${mahasiswa.major}`, 
            }
        ])

    return { data, error }
}

const get_mahasiswa_read_bulk = async () => {
    const { data, error } = await postgrest
        .from('table_mahasiswa')
        .select('mahasiswa_id, mahasiswa_nama')
        .order('mahasiswa_id', {ascending: true})

    return { data, error }
}

const get_mahasiswa_read_single = async (id) => {
    const { data, error } = await postgrest
        .from('table_mahasiswa')
        .select('*')
        .eq('mahasiswa_id', id)

    return { data, error }
}

const put_mahasiswa_update = async (mahasiswa, id) => {
    const { data, error } = await postgrest
        .from('table_mahasiswa')
        .update([
            {
                mahasiswa_nama: `${mahasiswa.nama}`, 
                mahasiswa_gender: `${mahasiswa.gender}`, 
                mahasiswa_birthdate: `${mahasiswa.birth}`, 
                mahasiswa_period: mahasiswa.period, 
                mahasiswa_faculty: `${mahasiswa.faculty}`, 
                mahasiswa_major: `${mahasiswa.major}`,
            }
        ])
        .eq('mahasiswa_id', id)

    return { data, error }
}

const delete_mahasiswa_remove_bulk = async (id) => {
    const { data, error } = await postgrest
        .from('table_mahasiswa')
        .delete()

    return { data, error }
}

const delete_mahasiswa_remove_single = async (id) => {
    const { data, error } = await postgrest
        .from('table_mahasiswa')
        .delete()
        .eq('mahasiswa_id', id)

    return { data, error }
}

api_service.options('*', cors({ origin: ["http://0.0.0.0", raw_server_url, "http://127.0.0.1"], methods: ['GET', 'PUT', 'POST', 'DELETE'], allowedHeaders: 'Content-Type', optionsSuccessStatus: 200 }))

api_service.use(bodyParser.json({ type: '*/*'}))

api_service.get("/", async (req, res) => {
    res.status(200).send("Sample API is Running & so does Express.")
})

api_service.get("/mahasiswa/", async (req, res) => {
    const { data, error } = await get_mahasiswa_read_bulk()

    if (error) {
        res.status(401)
    } else {
        res.status(200).jsonp({result: data})
    }
})

api_service.get("/mahasiswa/:mahasiswa_nim", async (req, res) => {
    if (req.params.mahasiswa_nim) {
        const { data, error } = await get_mahasiswa_read_single(req.params.mahasiswa_nim)

        if (error) {
            res.status(401)
        } else {
            res.status(200).jsonp({result: data})
        }
    }
})

api_service.post("/mahasiswa/create", async (req, res) => {
    if (req.body) {
        const { data, error } = await post_mahasiswa_create(req.body)

        if (error) {
            res.status(401)
        } else {
            res.status(200).jsonp({result: data})
        }
    } else {
        res.status(403)
    }
})

api_service.put("/mahasiswa/:mahasiswa_nim/update", async (req, res) => {
    if (req.body) {
        const { data, error } = await put_mahasiswa_update(req.body, req.params.mahasiswa_nim)

        if (error) {
            res.status(401)
        } else {
            res.status(200).jsonp({result: data})
        }
    } else {
        res.status(403)
    }
})

api_service.delete("/mahasiswa/:mahasiswa_nim/delete", async (req, res) => {
    if (req.params.mahasiswa_nim) {
        const { data, error } = await delete_mahasiswa_remove_single(req.params.mahasiswa_nim)

        if (error) {
            res.status(401)
        } else {
            res.status(200).jsonp({result: data})
        }
    } else {
        res.status(403)
    }
})

api_service.delete("/mahasiswa/reset", async (req, res) => {
    const { data, error } = await delete_mahasiswa_remove_bulk()

    if (error) {
        res.status(401)
    } else {
        res.status(200).jsonp({result: data})
    }
})

api_service.listen(global_port, () => {
    console.log(`Sample API is Running on port ${global_port}`)
})
