CREATE TABLE public.table_mahasiswa (
	mahasiswa_id serial8 NOT NULL,
	mahasiswa_nama varchar NOT NULL,
	mahasiswa_gender text NOT NULL,
	mahasiswa_birthdate date NOT NULL,
	mahasiswa_period int NOT NULL DEFAULT 1970,
	mahasiswa_faculty varchar NOT NULL,
	mahasiswa_major varchar NOT NULL,
	mahasiswa_creation timestamp NOT NULL DEFAULT now(),
	mahasiswa_updated timestamp NOT NULL DEFAULT now(),
	CONSTRAINT table_mahasiswa_pk PRIMARY KEY (mahasiswa_id)
);
