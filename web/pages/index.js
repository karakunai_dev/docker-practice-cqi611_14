import axios from 'axios'
import { useState } from 'react'
import Navbar from '../components/Navbar'
import Link from 'next/link'

// export async function getServerSideProps() {
  

//   return {
//     props: {
      
//     }
//   }
// }
const raw_server_url = process.env.NEXT_PUBLIC_GLOBAL_BASE_URL
const raw_server_port = process.env.NEXT_PUBLIC_EXPOSED_PORT_GATEWAY_API
const global_url = `${raw_server_url}:${raw_server_port}`
const axiosInstance = axios.create({
  baseURL: global_url
})

const postDummy = () => {
  axiosInstance({
    method: 'POST',
    url: '/mahasiswa/create',
    data: {
      nama: `Ronan Harris`, 
      gender: `Male`, 
      birth: `1999-07-18`, 
      period: 2018, 
      faculty: `Computer Science`, 
      major: `Information Technology`,
    },
  })
}

const IndexPage = () => {

  const [sent, setSent] = useState(false)
  const brbrb = () => {
    try {
      postDummy()
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <>
      <Navbar />
      <div className="container lg:mx-auto mx-6 flex items-center justify-center h-screen">
        <div className="block text-gray-900">
          <h1 className="text-7xl font-extralight">
            Sample CRUD App
          </h1>
          <p className="text-2xl">
            By <strong>Ronan Harris</strong>
          </p>
          <small className="font-bold">
            Version 1.0.0
          </small>
          <div className="my-6">
            <Link href="/about">
              <button className="bg-gray-900 text-white text-lg font-bold p-3 hover:shadow-lg hover:bg-white hover:text-gray-900 transform transition-colors">
                What is this ?
              </button>
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}

export default IndexPage;
