import Navbar from "../../components/Navbar"
import axios from 'axios'
import Link from 'next/link'
import { useState } from 'react'
import { useRouter } from 'next/router'
import { route } from "next/dist/next-server/server/router"

const raw_server_url = process.env.NEXT_PUBLIC_GLOBAL_BASE_URL
const raw_server_port = process.env.NEXT_PUBLIC_EXPOSED_PORT_GATEWAY_API
const global_url = `${raw_server_url}:${raw_server_port}`
const axiosInstance = axios.create({
    baseURL: global_url
})

const MahasiswaCreatePage = () => {

    const [ dataNama, setDataNama ] = useState("")
    const [ dataGender, setDataGender ] = useState("")
    const [ dataBirth, setDataBirth ] = useState("")
    const [ dataPeriod, setDataPeriod ] = useState(0)
    const [ dataFaculty, setDataFaculty ] = useState("")
    const [ dataMajor, setDataMajor ] = useState("")
    const [ isCreated, setIsCreated ] = useState(false)
    const router = useRouter()
    
    var dataMahasiswa = {
        nama: dataNama || "Nama...",
        gender: dataGender || "Male",
        birth: dataBirth || "2000-02-20",
        period: parseInt(dataPeriod) || 2000,
        faculty: dataFaculty || "Computer Science",
        major: dataMajor || "Teknik Informatika"
    }

    const formSubmit = (event) => {
        event.preventDefault()

        if(!event.target.checkValidity()) {
            return
        }
        
        sendForm()
        setIsCreated(true)
    }

    const sendForm = () => {
        axiosInstance({
            method: 'POST',
            url: `/mahasiswa/create`,
            data: dataMahasiswa
        })
    }

    return (
        <>
            <Navbar />
            <div className="container lg:mx-auto mx-6 flex-row items-center justify-center mt-24">
                <div className="block text-gray-900 text-center">
                    <h1 className="text-5xl">
                        Form Mahasiswa Baru
                    </h1>
                    <p className="text-2xl mt-3">
                        Mohon Lengkapi Form Berikut Untuk Dapat Diproses.
                    </p>
                </div>
                <form className="text-gray-900 border-2 border-gray-900 p-6 text-xl font-light mt-6" noValidate onSubmit={formSubmit}>
                    <div className="flex my-6">
                        <label htmlFor="nama" className="font-bold w-full p-2">Nama Mahasiswa</label>
                        <input type="text" name="nama" id="nama" className="w-full p-2" required autoComplete="off" defaultValue="Nama.." onChange={(e) => setDataNama(e.target.value)} />
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="gender" className="font-bold w-full p-2">Jenis Kelamin</label>
                        <select name="gender" id="gender" className="w-full p-2" required autoComplete="off" defaultValue="Male" onChange={(e) => setDataGender(e.target.value)}>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Other</option>
                        </select>
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="birth" className="font-bold w-full p-2">Tanggal Lahir</label>
                        <input type="date" name="birth" id="birth" className="w-full p-2" required autoComplete="off" defaultValue="2000-02-20" onChange={(e) => setDataBirth(e.target.value)} />
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="period" className="font-bold w-full p-2">Periode</label>
                        <input type="number" name="period" id="period" className="w-full p-2" required autoComplete="off" defaultValue={2000} onChange={(e) => setDataPeriod(e.target.value)} />
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="faculty" className="font-bold w-full p-2">Fakultas</label>
                        <select name="faculty" id="faculty" className="w-full p-2" required autoComplete="off" defaultValue="Computer Science" onChange={(e) => setDataFaculty(e.target.value)}>
                            <option>Computer Science</option>
                            <option>Others</option>
                        </select>
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="major" className="font-bold w-full p-2">Jurusan</label>
                        <select name="major" id="major" className="w-full p-2" required autoComplete="off" defaultValue="Teknik Informatika" onChange={(e) => setDataMajor(e.target.value)}>
                            <option>Teknik Informatika</option>
                            <option>Sistem Informasi</option>
                        </select>
                    </div>
                    {
                        isCreated ?
                        <></>
                        :
                        <button id="button_create" className="bg-gray-700 text-white text-lg font-bold p-3 hover:shadow-lg hover:bg-white hover:text-gray-900 transform transition-colors">
                            Simpan Perubahan
                        </button>
                    }
                </form>
                {
                    isCreated ?
                    <Link href="/mahasiswa">
                        <button className="bg-gray-900 text-white text-lg font-bold p-3 hover:shadow-lg hover:bg-white hover:text-gray-900 transform transition-colors">
                            Kembali Ke Daftar Mahasiswa
                        </button>
                    </Link>
                    :
                    <></>
                }
            </div>
        </>
    )
}

export default MahasiswaCreatePage;