import Navbar from "../../components/Navbar"
import MahasiswaTable from "../../components/MahasiswaTable"
import axios from 'axios'
import Link from "next/link"

const raw_server_url = process.env.NEXT_PUBLIC_GLOBAL_BASE_URL
const raw_server_port = process.env.NEXT_PUBLIC_EXPOSED_PORT_GATEWAY_API
const global_url = `${raw_server_url}:${raw_server_port}`
const axiosInstance = axios.create({
    baseURL: global_url
})

export async function getServerSideProps(context) {
    const fetchData = await (await axiosInstance.get('/mahasiswa')).data
    
    console.log(fetchData)

    if (!fetchData) {
        return {
            notFound: true,
        }
    }

    return {
        props: {
            fetchData
        }
    }
}

const MahasiswaIndexPage = ({ fetchData }) => {
    return (
        <>
            <Navbar />
            <div className="container lg:mx-auto mx-6 flex-row items-center justify-center mt-24">
                <div className="block text-gray-900 text-center">
                    <h1 className="text-5xl">
                        Tabel Mahasiswa
                    </h1>
                    <p className="text-2xl mt-3">
                        Berikut Ini Daftar Mahasiswa Pada Basis Data Mahasiswa.
                    </p>
                </div>
                <Link href="/mahasiswa/create">
                    <button id="button_create" className="bg-gray-500 text-white text-lg font-bold p-3 hover:shadow-lg hover:bg-white hover:text-gray-900 transform transition-colors flex mx-auto mt-6">
                        Tambah Mahasiswa
                    </button>
                </Link>
                <MahasiswaTable arrayData={fetchData.result} />
            </div>
        </>
    )
}

export default MahasiswaIndexPage;