import { useState } from 'react'
import Link from 'next/link'

const MahasiswaTableItem = ({ itemData, currentIndex }) => {
    return (
        <>
            <tr className="bg-white hover:bg-gray-400">
                <td className="p-3 text-center">
                    {currentIndex + 1}
                </td>
                <td className="p-3 text-center">
                    {itemData.mahasiswa_id}
                </td>
                <td className="p-3">
                    {itemData.mahasiswa_nama}
                </td>
                <td className="p-3">
                    <Link href={`/mahasiswa/${itemData.mahasiswa_id}`}>
                        <button className="bg-gray-900 text-white font-bold p-2 hover:bg-white hover:text-gray-900 transform transition-colors">
                            {"->"}
                        </button>
                    </Link>
                </td>
            </tr>
        </>
    )
}

export default MahasiswaTableItem;