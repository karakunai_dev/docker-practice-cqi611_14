import MahasiswaTableItem from "./MahasiswaTableItem"
import { useState } from 'react'

const MahasiswaTable = ({ arrayData }) => {

    const [ keyword, setKeyword ] = useState("")

    return (
        <>
            <div className="w-6/12 mx-auto mt-6">
                <input type="text" name="keyword" id="keyword" className="w-full p-3 border-2 border-gray-900 focus:outline-none focus:bg-gray-800 focus:text-white transform transition-colors" placeholder="Cari sebuah kata kunci..." required autoComplete="off" defaultValue="" onChange={(e) => setKeyword(e.target.value)} />
            </div>
            <table className="w-6/12 text-left text-lg text-gray-900 table-auto border-2 border-gray-900 mx-auto">
                <thead className="bg-gray-900 text-white">
                    <tr>
                        <th className="p-3 text-center">#</th>
                        <th className="p-3 text-center">ID</th>
                        <th className="p-3">Nama Lengkap</th>
                        <th className="p-3">Rincian</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        arrayData.filter((item) => (item.mahasiswa_id.toString().includes((keyword)) || item.mahasiswa_nama.toLowerCase().includes(keyword.toLowerCase()))).map((item, index) => (
                            <MahasiswaTableItem key={item.mahasiswa_id} currentIndex={index} itemData={item} />
                        ))
                    }
                </tbody>
            </table>
        </>
    )
}

export default MahasiswaTable;