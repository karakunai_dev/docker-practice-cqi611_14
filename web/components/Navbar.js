import Link from 'next/link'
import { useState, useEffect } from 'react'

const Navbar = ({ activeMenu }) => {

    const [ showShadow, setShowShadow ] = useState(false)
    const toggleShadow = () => window.scrollY > 32 ? setShowShadow(true) : setShowShadow(false)

    useEffect(() => {
        window.addEventListener('scroll', toggleShadow)
    })

    return (
        <>
            <div className={"fixed top-0 inset-x-0 z-10 bg-opacity-60 bg-white backdrop-filter backdrop-blur-lg text-gray-900 text-lg" + (showShadow ? " shadow-lg" : " shadow-none")}>
                <div className="container mx-auto pt-3 px-3">
                    <nav className="flex justify-around lg:w-full xl:w-6/12 xl:mx-auto">
                        <Link href="/">
                            <span className="text-center border-b-4 border-transparent hover:border-gray-800 pb-3 cursor-pointer">
                                Beranda
                            </span>
                        </Link>
                        <Link href="/mahasiswa">
                            <span className="text-center border-b-4 border-transparent hover:border-gray-800 pb-3 cursor-pointer">
                                Data Mahasiswa
                            </span>
                        </Link>
                        <Link href="/reset">
                            <span className="text-center border-b-4 border-transparent hover:border-gray-800 pb-3 cursor-pointer">
                                Reset Database
                            </span>
                        </Link>
                    </nav>
                </div>
            </div>
        </>
    )
}

export default Navbar;